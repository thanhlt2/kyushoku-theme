/**
 * All js script for recruitment page
 * @author ThanhLT2
 * 
 * @const TEMPLATE_URL : get url of current theme ( register at header as Global )
 */

(function ($) {
    var $global_current_page = 0;
    var $global_total_page   = 0;
    var $global_post_per_page = 0;
    var isFirstLoad = true;

    $(document).ready(function() {
        setEventHoverPag();
        $("#qtranslate-language-recruit-chooser").addClass("dropdown-menu dropdown-menu-pc");
        setEventPagination();

        var getHash = window.location.hash;
        if ( getHash.trim().length > 0 ) {
            // alert("co hash");
            if ( parseInt(getHash.replace("#", "")) > 1) {
                ajaxProcessingForRecruitment(getHash.replace("#", ""));
            }
        } else {
            ajaxProcessingForRecruitment(1);
        }
        
    });

    /**
     * set event when hover to button next and prev of
     * pagination
     */
    function setEventHoverPag() {
        $('.recrui-pagination ul li.prev a img').hover(function(){
            $(this).attr('src',TEMPLATE_URL+'/images/recruitment/icon-prev-hv.png');
            },function(){
            $(this).attr('src',TEMPLATE_URL+'/images/recruitment/icon-prev.png');
        });

        $('.recrui-pagination ul li.next a img').hover(function(){
            $(this).attr('src',TEMPLATE_URL+'/images/recruitment/icon-next-hv.png');
            },function(){
            $(this).attr('src',TEMPLATE_URL+'/images/recruitment/icon-next.png');
        });
    }

    function setEventPagination() {
        $(".recrui-pagination ul").on("click", "li", function(e) {
            var isHasData = true;
            e.preventDefault();
            var hashData = $(this).find("a").attr("href").replace("#","");
            if (hashData === "prev") {
                if ( $global_current_page > 1 && $global_current_page < $global_total_page ) {
                    hashData = $global_current_page - 1;
                } else {
                    isHasData = false;
                    return;
                }
            }
            if (hashData === "next") {
                if ( $global_current_page > 0 && $global_current_page < $global_total_page - 1 ) {
                    hashData = $global_current_page + 1;
                } else {
                    isHasData = false;
                    return;
                }
            }
            if ( isHasData ) {
                // ajaxProcessingForRecruitment(hashData.replace("#", ""));
                window.location.hash = "#"+hashData;
                ajaxProcessingForRecruitment(hashData);
                $(this).closest("ul").find("li").removeClass('active');
                $(this).closest("ul").find("a[href='#'"+ hashData +"]").closest("li").addClass("active");
                // $(this).addClass("active");
            }
        });
    }
    
    function ajaxProcessingForRecruitment(hash) {
        var hash = parseInt(hash);
        $('html, body').animate({scrollTop: 0}, 600);
        jQuery.ajax({
            url: ajax.ajaxRecruitment,
            type: "POST",
            data: {
                'action' : 'kyushoku_ajax_pagination_recruitment',
                'page_number' : hash
            },
            success: function(result) {
                console.log(result);
                if ( result.data !== null ) {
                    $global_current_page = result.data.current_page;  // set current page
                    $global_total_page = result.data.total_page;      // set total page

                    $(".list-work.row.row-recrui").empty().html(result.data.html_render);
                    pagination($global_current_page);
                }
            },
            error: function(err) {
                console.log(err);
            }
        })
    }

    function pagination($hash) {
        var ulPag = $(".recrui-pagination ul");
        $(".recrui-pagination ul li").removeClass("active");
        $(".recrui-pagination ul li a[href='#"+ $hash +"']").closest("li").addClass("active");
        var get_current_li = $(".recrui-pagination ul li.active").find("a").attr("href");
        var first_li_number = $(".recrui-pagination ul li.number:first").find("a").attr("href");
        var last_li_number = $(".recrui-pagination ul li.number:last").find("a").attr("href");

        // get_current_li.find("a").attr("href"));
        // console.log(first_li_number.find("a").attr("href"));
        // console.log(last_li_number.find("a").attr("href"));
        if ( get_current_li == last_li_number ) {
            ulPag.find("li.number").removeClass("active");
            ulPag.find("li.number").each(function (i) {
                if ( i === 0) {
                    $(this).addClass("active");
                }
                $(this).find("a").attr("href", "#"+ (parseInt(last_li_number.replace("#", "")) + i) );
                $(this).find("a").text(parseInt(last_li_number.replace("#", "")) + i);
            });
            // ulPag
        }
        if ( get_current_li == first_li_number) {
            ulPag.find("li.number").removeClass("active");
            var _length = ulPag.find("li.number").length();
            ulPag.find("li.number").each(function (j) {
                if ( j === _length) {
                    $(this).addClass("active");
                }
                $(this).find("a").attr("href", "#"+ (parseInt(first_li_number.replace("#", "")) - i) );
                $(this).find("a").text(parseInt(first_li_number.replace("#", "")) - i);
            });
        }
    }

    function printNumberOfPage() {
        
    }

})(jQuery);

