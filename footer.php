<?php
/**
 * The template for displaying the footer
 *
 * @package kyushoku-theme
 */

?>
<footer id="nav-footer">
    <nav class="navbar navbar-default navbar-static-top navbar-footermenu">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3">
                    <div class="footer-logo">
                        <a class="navbar-brand logo-brand" href="#">
                            <img src="<?php kyushoku_getTemplateUrl(); ?>/images/logo_footer.png" alt="Evolable Asia Agent" class="responsive">
                        </a>
                    </div>        
                </div>
                <div class="col-xs-12 col-sm-9 col-md-9">
                    <!-- Footer Menu -->
                    <?php 
						$menuFooter = array(
							'theme_location' => 'header-menu',
							'container' => 'ul',
							'container_class' => 'nav-footer',
							'menu_class'      => 'nav navbar-nav nav-footer text-uppercase'
						);
						kyushoku_getMenu($menuFooter);
					?>
                    <!-- // End Footer Menu -->
                </div>
            </div>
        </div>
    </nav>
    <div class="footer-info">
        <div class="container">
            <div class="row">
                <div class="col-xs-8 col-sm-6 col-md-8">
                    <address>
                        10F Shiba Daimon Centre, 1-10-11 Shiba Daimon, Minato Ku, Tokyo, Japan<br/>
                        Zip: 105-0012<br/>
                        Tel: 03-6880-9083<br/>
                        Fax: 03-6880-9201
                    </address>
                </div>
                <div class="col-xs-4 col-sm-6 col-md-4">
                    <div class="lang-block"><a href="#" class="btn btn-success btn-lang"><?php echo qtranxf_getLanguageName(); ?></a></div>
                    <div class="social-block">
                        <a href="#" class="fb-ico"><img src="<?php kyushoku_getTemplateUrl(); ?>/images/icon_facebook_footer.png" alt="Facebook" /></a>
                    </div>
                </div>
            </div>
            <div class="row pull-right top-page">
                <span>
                    <img src="<?php kyushoku_getTemplateUrl(); ?>/images/icon_topPage.png" alt="Top" title="Goto Top page">
                </span>
            </div>
        </div>
    </div>

    <div class="footer-copyright">
        <div class="container">
            <p class="text-center">
                Copyright EVOLABLE ASIA Co,. Ltd. All Rights Reserved.
            </p>
        </div>
    </div>
    
</footer>

<?php wp_footer(); ?>

</body>
</html>
