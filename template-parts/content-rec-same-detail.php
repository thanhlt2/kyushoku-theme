<?php 
/**
 * Get the same jobs with current job
 *
 **/
$argsJobs = array(
    'post_type'      => 'recruitment',
    'orderby'        => 'date',
    'hide_empty'     => false,
    'depth'          => 1,
    'post_status'    => 'publish',
    'order'          => 'DESC',
    'posts_per_page' => 12
);
$wp_query1 = new WP_Query( $argsJobs );

?>
<section data-wow-delay="0.4s" class="wr-list-other wow fadeInUp animated">
    <div class="clearfix">   
        <div class="container">
            <div class="row row-recrui-detail">
                <div class="title-view-other">
                    <h3><a href="#"><?php echo __('[:ja]似ている求人[:en]Same jobs[:vi]Các công việc tương tự') ?></a></h3>
                        <div class="more-other">
                            <a class="btn-more-other" href="<?php echo get_home_url().'/recruitment/'; ?>">
                                <img src="<?php echo kyushoku_getTemplateUrl().__('[:ja]/images/recruitment-detail/other-ja.png[:en]/images/recruitment-detail/other-en.png[:vi]/images/recruitment-detail/other-vi.png') ?>" alt="Other">
                            </a>
                        </div>
                </div>
            </div>
            <div class="row row-list-slider">
                <div class="owl-carousel owl-theme list-work">
                    <div class="item list-work-item wow fadeInUp animated" data-wow-delay="0.4s">
                        <a href="tuyendung_chitiet.html" class="box-work" title="">
                            <div class="box-work-img"><img src="<?php kyushoku_getTemplateUrl(); ?>/images/recruitment/img-1.png" alt="" /></div>
                            <figcaption><span>Bridge SE (Mobile Application Project Development) </span></figcaption>
                            <div class="cmt-work">
                                <p class="cmt-map">Tokyo</p>
                                <p class="cmt-sal">Ước tính thu nhập hằng năm: 3-4 triệu yên</p>
                                <div class="tit-skill">
                                    <span>Kỹ năng</span>
                                </div>
                                <div class="list-skill">
                                    <span>Japanese</span>
                                    <span>Mobile applications</span>
                                </div>
                                <span class="readmore">Xem chi tiết >></span>
                            </div>
                        </a>
                    </div>
                    <div class="item list-work-item wow fadeInUp animated" data-wow-delay="0.4s">
                        <a href="tuyendung_chitiet.html" class="box-work" title="">
                            <div class="box-work-img"><img src="<?php kyushoku_getTemplateUrl(); ?>/images/recruitment/img-1.png" alt="" /></div>
                            <figcaption><span>Quản lý phát triển doanh nghiệp</span></figcaption>
                            <div class="cmt-work">
                                <p class="cmt-map">TP. Hồ Chí Minh</p>
                                <p class="cmt-sal">Thu nhập: 200,000 yên/ tháng + phụ cấp bằng cấp + tiền thưởng giữa năm</p>
                                <div class="tit-skill">
                                    <span>Kỹ năng</span>
                                </div>
                                <div class="list-skill">
                                    <span>Japanese</span>
                                    <span>Sales experience</span>
                                    <span>Management experience</span>
                                </div>
                                <span class="readmore">Xem chi tiết >></span>
                            </div>
                        </a>
                    </div>
                    <div class="item list-work-item wow fadeInUp animated" data-wow-delay="0.4s">
                        <a href="tuyendung_chitiet.html" class="box-work" title="">
                            <div class="box-work-img"><img src="<?php kyushoku_getTemplateUrl(); ?>/images/recruitment/img-1.png" alt="" /></div>
                            <figcaption><span>Lập trình viên (làm việc tại Campuchia)</span></figcaption>
                            <div class="cmt-work">
                                <p class="cmt-map">Tokyo</p>
                                <p class="cmt-sal">Thương lượng</p>
                                <div class="tit-skill">
                                    <span>Kỹ năng</span>
                                </div>
                                <div class="list-skill">
                                    <span>HTML5 & CSS</span>
                                    <span>Javascript</span>
                                    <span>Java</span>
                                    <span>PHP</span>
                                    <span>Ruby</span>
                                </div>
                                <span class="readmore">Xem chi tiết >></span>
                            </div>
                        </a>
                    </div>
                    <div class="item list-work-item wow fadeInUp animated" data-wow-delay="0.4s">
                        <a href="tuyendung_chitiet.html" class="box-work" title="">
                            <div class="box-work-img"><img src="<?php kyushoku_getTemplateUrl(); ?>/images/recruitment/img-1.png" alt="" /></div>
                            <figcaption><span>Kỹ sư lập trình Web (lập trình cho Web công ty)</span></figcaption>
                            <div class="cmt-work">
                                <p class="cmt-map">Tokyo</p>
                                <p class="cmt-sal">3.5 triệu yen - 5.5 triệu yen *Dựa trên kỹ năng và kinh nghiệm</p>
                                <div class="tit-skill">
                                    <span>Kỹ năng</span>
                                </div>
                                <div class="list-skill">
                                    <span>HTML5 & CSS</span>
                                    <span>Javascript</span>
                                    <span>Java</span>
                                    <span>PHP</span>
                                    <span>Ruby</span>
                                </div>
                                <span class="readmore">Xem chi tiết >></span>
                            </div>
                        </a>
                    </div>
                    <div class="item list-work-item wow fadeInUp animated" data-wow-delay="0.4s">
                        <a href="tuyendung_chitiet.html" class="box-work" title="">
                            <div class="box-work-img"><img src="<?php kyushoku_getTemplateUrl(); ?>/images/recruitment/img-1.png" alt="" /></div>
                            <figcaption><span>Bridge SE (Mobile Application Project Development)</span></figcaption>
                            <div class="cmt-work">
                                <p class="cmt-map">Tokyo</p>
                                <p class="cmt-sal">Ước tính thu nhập hằng năm: 3-4 triệu yên</p>
                                <div class="tit-skill">
                                    <span>Kỹ năng</span>
                                </div>
                                <div class="list-skill">
                                    <span>Japanese</span>
                                    <span>Mobile applications</span>
                                </div>
                                <span class="readmore">Xem chi tiết >></span>
                            </div>
                        </a>
                    </div>                   
     
                </div>
            </div>

        </div>
    </div>
</section>