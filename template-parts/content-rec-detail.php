<?php
/**
 * Template part for displaying jobs information for recruitment
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kyushoku-theme
 */

?>
<!--- wr-breadcrumb-sub -->
<div class="wr-breadcrumb-sub clearfix">
    <div class="container">
        <div class="row row-recrui">
            <div class="cont-breadcrumb-sub">
                <div class="breadcrumb-sub"><h3><?php echo __('[:ja]Recruitment[:en]Recruitment[:vi]Tuyển dụng') ?></h3></div>
                <div class=""><h3><?php echo __('[:ja]Recruitment Position[:en]Recruitment Position[:vi]các vị trí tuyển dụng') ?></h3></div>
                <!--- LANGUAGE-->
                <div class="btn-group">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo qtranxf_getLanguageName(); ?> <span class="caret"></span>
                </button>
                <?php
                    if(function_exists('qtranxf_generateLanguageSelectCode')){
                        qtranxf_generateLanguageSelectCode(array(
                            'format' => 'custom',
                            'id' => 'qtranslate-language-rec',
                        ));
                    }
                ?>
                </div>
            </div>	
        </div>
    </div>
</div>

<!-- CONTENT DETAIL -->
<section class="wr-detail">
    <div class="wr-title clearfix">   
        <div class="container">
            <div class="row row-recrui-detail">
                <h3><?php the_title(); ?></h3>
                <div class="wr-btn-send-top">
                    <a class="btn-send-top" href="apply_job.html" /></a>
                </div>
                <div class="wr-btn-send-top-rs">
                    <a class="btn-send-top" href="apply_job.html" /><?php echo __('[:ja]Apply job[:en]Apply job[:vi]Ứng tuyển', 'kyushoku'); ?> <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>

    <?php
        // get all data from Block Info
        $contents_of_work = '';
        $expectations = '';
        $employment_status = '';
        $work_location = '';
        $estimated_annual_income = '';
        $working_hours = '';
        $welfare = '';
        $holiday_vacation = '';
        $another_field_1 = '';
        $another_field_2 = '';

        $array_sub_field = array('仕事の内容','求める経験','雇用形態','勤務地','想定年収','勤務時間','休日/休暇','福利厚生','選考プロセス');
        if ( have_rows('block_info') ) :
            while ( have_rows('block_info')) : the_row();
    
                $tname = "description_1";
                $title = get_sub_field('title');
                if ($title == '企業・求人概要') {
                    $tname = "description_1";
                } else if ($title == '応募条件') {
                    $tname = "description_2";
                } else if ($title == '勤務・就業規定・その他情報') {
                    $tname = "description_3";
                } else if ($title == '募集要項') {
                    $tname = "description_4";
                }

                while (have_rows($tname)): the_row();
                    // echo '<p class="label label-danger">'. get_sub_field('sub_title').'</p>';
                    // echo '<div>'. get_sub_field('content').'</div>';

                    switch( get_sub_field('sub_title') ) {
                        case($array_sub_field[0]):
                            $contents_of_work = get_sub_field('content');
                        break;
                        case($array_sub_field[1]):
                            $expectations = get_sub_field('content');
                        break;
                        case($array_sub_field[2]):
                            $employment_status = get_sub_field('content');
                        break;
                        case($array_sub_field[3]):
                            $work_location = get_sub_field('content');
                        break;
                        case($array_sub_field[4]):
                            $estimated_annual_income = get_sub_field('content');
                        break;
                        case($array_sub_field[5]):
                            $working_hours = get_sub_field('content');
                        break;
                        case($array_sub_field[6]):
                            $welfare = get_sub_field('content');
                        break;
                        case($array_sub_field[7]):
                            $holiday_vacation = get_sub_field('content');
                        break;
                        case($array_sub_field[8]):
                            $another_field_1 = get_sub_field('content');
                        break;
                        default: 
                            $another_field_2 = get_sub_field('content');
                        break;
                    }
                endwhile;

            endwhile;
        endif;
    ?>

    <div class="container content-detail">
        <div class="row row-recrui-detail">
            <div class="img-detail">
                <img src="<?php 
                    if ( get_field('default_image') ) {
                        echo get_field('default_image'); 
                    }
                    else {
                        echo kyushoku_getTemplateUrl().'/images/recruitment-detail/img-dt.png';
                    }
                
                ?>" alt="<?php echo get_field('sub_title') ?>" class="img-responsive" />
                <div style="clear: both"></div>
                <div class="row-recrui-detail title-rs">
                    <h3><?php the_title(); ?></h3>
                </div>
                 <div style="clear: both"></div>
                <div class="code-work code-work-rs">
                    <p><?php echo __('[:ja]求人No[:en]Jobs Code[:vi]Mã công việc') ?> <span><?php echo 'No.'.get_field('job_no') ?></span></p>
                </div>
            </div>
            <div class="info-detail">
                <div class="code-work code-work-rs-sm">
                    <p><?php echo __('[:ja]求人No[:en]Jobs Code[:vi]Mã công việc') ?> <span><?php echo 'No.'.get_field('job_no') ?></span></p>
                </div>
                <div style="clear: both"></div>
                <div class="row-dt-1">
                    <?php 
                        $term_locations = get_the_terms($post->ID, 'job-location');
                    ?>
                    <p>
                        <?php 
                        if (is_array($term_locations)):
                            foreach($term_locations as $location) :
                                echo $location->name . ", ";
                            endforeach;
                        endif;
                        ?>
                    </p>
                </div>
                <div class="row-dt-2"><p><?php echo get_field('salary') ?></p></div>
                <div class="row-dt-3"><?php echo $working_hours; ?></div>
                
                <div class="row-dt-4"><p><?php echo $welfare; echo '<br/>'.$holiday_vacation; ?></p></div>
            </div>
            <div class="code-work">
                <p><?php echo __('[:ja]求人No[:en]Jobs Code[:vi]Mã công việc') ?> <span><?php echo 'No.'.get_field('job_no') ?></span></p>
            </div>
        </div>

        

        <div class="row row-recrui-detail content-detail-main">
            <div data-wow-delay="0.4s" class="cate-wr-content cate-wr-content-open wow fadeInUp animated">
                <div class="box-detail-1">
                    <div class="cate-box-title"><h4><?php echo __('[:ja]仕事内容[:en]Jobs Description[:vi]mô tả công việc') ?></h4></div>
                </div>
                <div class="cate-box-content">
                    <?php
                        echo $contents_of_work;
                     ?>
                </div>
            </div>
            <div data-wow-delay="0.4s" class="cate-wr-content wow fadeInUp animated">
                <div class="box-detail-1">
                    <div class="cate-box-title"><h4><?php echo __('[:ja]必要経験[:en]Experience[:vi]Yêu cầu kinh nghiệm') ?></h4></div>
                </div>
                <div class="cate-box-content">
                    <?php
                        echo $expectations;
                     ?>
                </div> 
            </div>
            <div data-wow-delay="0.4s" class="cate-wr-content wow fadeInUp animated">
                <div class="box-detail-1">
                    <div class="cate-box-title"><h4><?php echo __('[:ja]事業内容[:en]Company Information[:vi]Thông tin công ty') ?></h4></div>
                </div>
                <div class="cate-box-content">
                    <?php echo get_field('content') ?>
                </div>
            </div>
            <div data-wow-delay="0.4s" class="cate-wr-content wow fadeInUp animated">
                <div class="box-detail-1">
                    <div class="cate-box-title"><h4><?php echo __('[:ja]勤務地[:en]Working location[:vi]Nơi làm việc') ?></h4></div>
                </div>
                <div class="cate-box-content">
                    <?php echo $work_location ?>
                </div>
            </div>
        </div>

        <div style="clear: both"></div>

        <div class="wr-btn-send-bot">
            <a class="btn-send-bot" href="apply_job.html">
                <img src="<?php echo kyushoku_getTemplateUrl().__('[:ja]/images/recruitment-detail/btn-send-bot-ja.png[:en]/images/recruitment-detail/btn-send-bot-en.png[:vi]/images/recruitment-detail/btn-send-bot-vi.png') ?>" alt="">
            </a>
        </div>
    </div>
</section>

<div style="clear: both"></div>

<!-- /CONTENT DETAIL -->

<!-- LIST OTHER -->
<?php
    get_template_part( 'template-parts/content-rec-same-detail', get_post_type() );
?>