<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kyushoku-theme
 */

get_header(); ?>

<div id="top-banner" class="container-fluid">
    <div class="container">
        <h2 class="text-uppercase text-banner">
            Thương hiệu <span class="big-text">Tuyển dụng</span><br/>thu hút nhân tài
        </h2>
        <div id="select-lang" class="dropdown hidden-xs">
            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <?php echo qtranxf_getLanguageName(); ?>
                <span class="caret"></span>
            </button>
            <?php
                if(function_exists('qtranxf_generateLanguageSelectCode')){
                    qtranxf_generateLanguageSelectCode(array(
                        'format' => 'custom',
                        'id' => 'qtranslate-language-index',
                        'class' => 'dropdown-menu'
                    ));
                }
            ?>
        </div>
    </div>
</div>

<?php
// get_sidebar();
get_footer();
