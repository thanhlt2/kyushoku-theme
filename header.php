<?php
/**
 * The header for our theme
 *
 * @package kyushoku-theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<meta name="format-detection" content="telephone=no">
	<title>
		<?php 
			// echo $current_post_type;
			if ( !is_home() ) {
				if ( is_archive('recruitment') ) {
					echo __("[:ja]求人情報[:en]Recruitment[:vi]Tuyển dụng");
				} else {
					echo get_the_title( get_the_ID() ); 
				}
			}
			if(wp_title('', false)) {echo " │ ";} bloginfo('name');
		?>
	</title>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header id="masthead" class="site-header">
		<!-- Menu navigation on header -->

		<nav id="top-menu" class="navbar navbar-default navbar-static-top navbar-topmenu">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand logo-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src='<?php kyushoku_getTemplateUrl(); ?>/images/logo.png' alt="Evolable Asia Agent" class="responsive">
					</a>
					<div class="dropdown menu-lang visible-xs">
						<button class="btn btn-success dropdown-toggle text-uppercase" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<?php echo qtranxf_getLanguage(); ?>
							<span class="caret"></span>
						</button>
						 <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
							<li><a href="#" data-lang="vi-VN">VI</a></li>
							<li><a href="#" data-lang="en-US">EN</a></li>
							<li><a href="#" data-lang="ja-JP">JP</a></li>
						</ul> 
						<?php
							if(function_exists('qtranxf_generateLanguageSelectCode')){
								qtranxf_generateLanguageSelectCode(array(
									'format' => '',
									'id' => 'qtranslate-language-sp',
									'class' => 'dropdown-menu'
								));
							}

						?>
					</div>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<?php 
						$menuHeader = array(
							'theme_location' => 'header-menu',
							'container' => 'ul',
							'container_class' => 'nav-header',
							'menu_class'      => 'nav navbar-nav navbar-right text-uppercase'
						);
						kyushoku_getMenu($menuHeader);
					?>
				</div>
				<!--/.nav-collapse -->
			</div>
		</nav>
		
	</header><!-- #masthead -->

<?php
	if (function_exists('kyushoku_getTemplateUrl')) :
?>
<script>
	var TEMPLATE_URL = "<?php echo kyushoku_getTemplateUrl();?>";
</script>
<?php 
	endif;