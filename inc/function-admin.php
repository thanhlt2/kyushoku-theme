<?php 
/**
 * @package kyushoku-theme
 * @author ThanhLT2
 * kyushoku-theme functions and definitions
 **/

/**
 * Include custom postype for registering
 **/
include_once (dirname(__FILE__) . '/custom_post_type/cpt_register.php');

/**
 * Function definitions
 **/
if ( !function_exists('kyushoku_setup_admin') ) :

    /**
     * Set up default function for admin-wp
     **/
    function kyushoku_setup_admin() {
        /*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on kyushoku-theme, use a find and replace
		 * to change 'kyushoku' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'kyushoku', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		// register_nav_menus( array(
		// 	'menu-1' => esc_html__( 'Primary', 'kyushoku' ),
		// ) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'kyushoku_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
    }
endif;
add_action( 'after_setup_theme', 'kyushoku_setup_admin' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
// function kyushoku_content_width() {
// 	$GLOBALS['content_width'] = apply_filters( 'kyushoku_content_width', 640 );
// }
// add_action( 'after_setup_theme', 'kyushoku_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function kyushoku_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Multi language', 'kyushoku' ),
		'id'            => 'multilang-qtranslate',
		'description'   => esc_html__( 'Add widgets here.', 'kyushoku' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'kyushoku_widgets_init' );

/**
 * Function setup for menu 
 **/
if ( !function_exists('kyushoku_setupMenu') ) :
	function kyushoku_setupMenu() {
		register_nav_menus(array(
			'header-menu' => esc_html__('Header Menu', 'kyushoku'),
			'footer-menu' => esc_html__('Footer Menu', 'kyushoku')
		));
	}
endif;
add_action( 'after_setup_theme', 'kyushoku_setupMenu' );

/**
 * Function process ajax for recruiment page (pagination)
 *  - Registered at file /inc/enqueue.php -- line 35
 **/
function kyushoku_ajax_pagination_recruitment() {
	global $post;
	$htmlRender = ''; // All html code to display at client

	$post_per_page = 12;           // Number of posts per page
	$pages = 0;                    // Total pages
	$totalPost = 0;                // Total record of post_type
	$totalShowPagination = 3;      // Total show pagination
	$currentPage = 0;              // Current page

	// Depend on search condition
	$pageHash = intval($_POST["page_number"]);
	if ($pageHash === NULL) {
		$pageHash = 1;
	}
	$currentPage = $pageHash;
	// Get all jobs ( recruitment )
	$argsAll = array (
		'post_type'      => 'recruitment',
		'hide_empty'     => false,
		'post_status'    => 'publish',
		'posts_per_page' => -1
	);
	$wpQueryAll = new WP_Query( $argsAll );
	$totalPost = $wpQueryAll->post_count;                  // return total posts
	$pages = ceil( $totalPost / $post_per_page );        // return total pages

	// Get data with condition depend on pagination ( Phan trang )
	/**
	 * Lay cac jobs hien tai dua tren phan trang
	 **/
	if ( $pageHash <= $pages ) {
		$argsJobs = array(
			'post_type'      => 'recruitment',
			'orderby'        => 'date',
			'hide_empty'     => false,
			'depth'          => 1,
			'post_status'    => 'publish',
			'order'          => 'DESC',
			'posts_per_page' => $post_per_page,
			'paged'          => $pageHash
		);
		$wpQuery_Condition = new WP_Query( $argsJobs );

		if ( $wpQuery_Condition->have_posts() ) {
			while ( $wpQuery_Condition->have_posts() ) : $wpQuery_Condition->the_post();
				$htmlRender.='<div class="list-work-item col-lg-3 col-md-4 col-sm-6 col-xs-6 col-full wow fadeInUp animated" data-wow-delay="0.4s">';
					$link = get_the_permalink();                                            // dynamic data
				$htmlRender.='<a href="'.$link.'" class="box-work" title="">';
				$htmlRender.='<div class="box-work-img">';
					$imgUrl = get_template_directory_uri()."/images/recruitment/img-4.png"; // dynamic data
				if ( get_field('detail_image') ) {
					$imgUrl = get_field('detail_image');
				}
					$term_locations = get_the_terms($post->ID, 'job-location');             // dynamic data
					$term_positions = get_the_terms($post->ID, 'job-position');             // dynamic data
					$term_skills = get_the_terms($post->ID, 'job-skill');                   // dynamic data
				$htmlRender.='<img src="'.$imgUrl.'" alt="'.get_the_title( $post ).'" />';
				$htmlRender.='</div>';
				$htmlRender.='<figcaption><span>'.get_the_title( $post ).'</span></figcaption>';
				$htmlRender.='<div class="cmt-work"><p class="cmt-map">';
					if (is_array($term_locations)):
						foreach($term_locations as $location) :
							$htmlRender.= $location->name . ", ";
						endforeach;
					endif;
				$htmlRender.='</p>';
				$htmlRender.='<p class="cmt-sal">';
					if ( get_field('salary') ) :
						$htmlRender.= get_field('salary');
					endif;
				$htmlRender.='</p>';
				$htmlRender.='<div class="tit-skill">';
				$htmlRender.='<span>'.__('[:ja]Skills[:en]Skills[:vi]Kỹ năng', 'kyushoku').'</span>';
				$htmlRender.='</div>';
				$htmlRender.='<div class="list-skill">';
					if (is_array($term_skills)):
						foreach($term_skills as $skill_item) :
							$htmlRender.= "<span>". $skill_item->name ."</span>";
						endforeach;
					endif;
				$htmlRender.='</div>';
				$htmlRender.='<span class="readmore">'.__('[:ja]Detail >>[:en]Detail >>[:vi]Xem chi tiết >>', 'kyushoku').'</span>';
				$htmlRender.='</div>';
				$htmlRender.='</a>';
				$htmlRender.='</div>';
			endwhile;
		}
	}

	$dataReturn = array (
		'html_render'    => $htmlRender,
		'current_page'   => $currentPage,
		'total_page'     => $pages,
		'post_per_page'  => $post_per_page
	);
	wp_send_json_success( $dataReturn );
}
add_action('wp_ajax_kyushoku_ajax_pagination_recruitment','kyushoku_ajax_pagination_recruitment');