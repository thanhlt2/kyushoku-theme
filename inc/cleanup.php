<?php
/**
 * Remove version string from js and css
 **/
function kyushoku_remove_wp_version_strings($src){
	global $wp_version;
	parse_str(parse_url($src, PHP_URL_QUERY), $query);
	if(!empty($query['ver']) && $query['ver']){
		$src = remove_query_arg('ver', $src);
	}

	return $src;
}

add_filter('style_loader_src', 'kyushoku_remove_wp_version_strings');
add_filter('script_loader_src', 'kyushoku_remove_wp_version_strings');

/**
 * Remove metatag generator from header
 **/
function kyushoku_remove_meta_version() {
	return '';
}
add_filter( 'the_generator', 'kyushoku_remove_meta_version' );