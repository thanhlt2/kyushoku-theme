<?php
/**
 * @author ThanhLT2
 **/

/**
 * Enqueue scripts and styles.
 */
function kyushoku_scripts() {
	wp_enqueue_style( 'kyushoku-style', get_stylesheet_uri() );

	wp_enqueue_style('kyushoku-index-page', get_template_directory_uri() . '/css/index_styles.min.css', array(), '1.0', 'all');
	wp_enqueue_script( 'kyushoku-lib-jquery', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', array(), '', true );
	wp_enqueue_script( 'kyushoku-lib-bootstrap', get_template_directory_uri() . '/js/lib/combined.js', array(), '', true );
	wp_enqueue_script( 'kyushoku-script-mainjs', get_template_directory_uri() . '/js/main.js', array(), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/**
	 * style and script for page/archive recruitment
	 **/
	if ( is_page('recruitment') || is_archive('recruitment') ) {
		wp_enqueue_style('kyushoku-recruitment-animate', get_template_directory_uri() . '/css/recrui/animate.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-page', get_template_directory_uri() . '/css/recrui/recruitment.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-responsive', get_template_directory_uri() . '/css/recrui/responsive.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-myriad', get_template_directory_uri() . '/css/recrui/myriad.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', array(), '1.0', 'all');

		wp_enqueue_script( 'kyushoku-recruitment-script-wow', get_template_directory_uri() . '/js/recrui/wow.js', array(), '', true );
		wp_enqueue_script( 'kyushoku-recruitment-script-wow-sc', get_template_directory_uri() . '/js/recrui/wow-lib.js', array(), '', true );
		// wp_enqueue_script( 'kyushoku-recruitment-script-paging', get_template_directory_uri() . '/js/recrui/jquery.paging.min.js', array(), '', true );
		wp_enqueue_script( 'kyushoku-recruitment-script', get_template_directory_uri() . '/js/recrui/script-recrui.js', array(), '', true );
		// register ajax processing for recruitment
		wp_localize_script( 'kyushoku-recruitment-script', 'ajax', array( 'ajaxRecruitment' => admin_url( 'admin-ajax.php' ) ) );
	}

	if ( is_page('jobs') ) {
		wp_enqueue_style('kyushoku-recruitment-animate', get_template_directory_uri() . '/css/recrui/animate.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-page', get_template_directory_uri() . '/css/recrui/recruitment.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-responsive', get_template_directory_uri() . '/css/recrui/responsive.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-myriad', get_template_directory_uri() . '/css/recrui/myriad.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-1', get_template_directory_uri() . '/css/recrui-detail/recruitment-detail.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-2', get_template_directory_uri() . '/css/recrui-detail/recruitment-detail-responsive.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', array(), '1.0', 'all');

		wp_enqueue_style('kyushoku-recruitment-3', get_template_directory_uri() . '/css/recrui-detail/owl.carousel.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-4', get_template_directory_uri() . '/css/recrui-detail/owl.theme.default.css', array(), '1.0', 'all');
		wp_enqueue_style('kyushoku-recruitment-5', get_template_directory_uri() . '/css/recrui-detail/detail-responsive.css', array(), '1.0', 'all');

		wp_enqueue_script( 'kyushoku-recruitment-script-wow', get_template_directory_uri() . '/js/recrui/wow.js', array(), '', true );
		wp_enqueue_script( 'kyushoku-recruitment-script-wow-sc', get_template_directory_uri() . '/js/recrui/wow-lib.js', array(), '', true );
		wp_enqueue_script( 'kyushoku-recruitment-script-wow-sc', get_template_directory_uri() . '/js/recrui-detail/owl.carousel.min.js', array(), '', true );
		wp_enqueue_script( 'kyushoku-recruitment-script', get_template_directory_uri() . '/js/recrui-detail/script-recrui-detail.js', array(), '', true );
	}
}
add_action( 'wp_enqueue_scripts', 'kyushoku_scripts' );