<?php

/*
 * Author: haudv
 * Template Name: Candidate File Download
 * 
 */
global $wp_query;
global $wpdb;

if (current_user_can('manage_options')) {
    //
    if (isset($_GET['candidate'])) {
        if (is_numeric($_GET['candidate'])) {
            $id = $_GET['candidate'];
            $table_name = $wpdb->prefix . 'job_candidates';
            $list_candidates = $wpdb->get_results(
                    ""
                    . " SELECT * "
                    . " FROM  $table_name "
                    . " WHERE id =  $id "
            );
            $post = $list_candidates[0];
            switch ($_GET['attach']) {
                case 'resume':
                    $attach_file = $post->resume_file;
                    break;
                case 'cv':
                    $attach_file = $post->cv_file;
                    break;                
                default:
                    $attach_file = $post->other_file;
                    break;
            }
            $ext = substr(strrchr($attach_file, '.'), 1);
            $clean_name = sanitize_title($post->name . '-'.$_GET['attach']) . '.' . $ext;
            //
            $path = str_replace(home_url(), '', $attach_file);
            $attach_file_path = WP_CONTENT_DIR . str_replace('/wp-content', '', $path);
            //
            header('Content-Description: File Transfer');
            header('Content-Type: application/force-download');
            header("Content-Disposition: attachment; filename=\"" . $clean_name . "\";");
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($attach_file_path));
            ob_clean();
            flush();
            readfile($attach_file_path);
            exit();
        }
    }
}
