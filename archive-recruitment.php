<?php
get_header();
?>
<!--- wr-breadcrumb-sub -->
<div class="wr-breadcrumb-sub clearfix">
	<div class="container">
		<div class="row row-recrui">
			<div class="cont-breadcrumb-sub">
				<div class="breadcrumb-sub"><h3 class="text-uppercase">Tuyển dụng</h3></div>
				<div class=""><h3 class="text-uppercase">các vị trí tuyển dụng</h3></div>
				<!--- LANGUAGE-->
				<div class="btn-group">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo qtranxf_getLanguageName(); ?>  <span class="caret"></span>
                    </button>
                    <?php
                        if(function_exists('qtranxf_generateLanguageSelectCode')){
                            qtranxf_generateLanguageSelectCode(array(
                                'format' => 'custom',
                                'id' => 'qtranslate-language-recruit',
                                'class' => 'dropdown-menu'
                            ));
                        }
                    ?>
                </div>
                
			</div>	
		</div>
	</div>
</div>
<!--- /wr-breadcrumb-sub -->

<!--- CONTENT -->
<?php
	$ar_location = get_terms('job-location', array('slug'=>trim(isset($_GET['location'])?$_GET['location']:'')));
	$location = count($ar_location)==1?$ar_location[0]:null;
	$ar_skills = get_terms('job-skill', array('slug'=>trim(isset($_GET['skill'])?$_GET['skill']:'')));
	$skill = count($ar_skill)==1?$ar_skill[0]:null;
?>
<section class="container wr-recrui">
	<form action="" method="">
		<div class="row row-recrui">
			<!--- SEARCH ADVANCED -->
			<div class="wr-select-adv">
				<div class="box-sl">
					<select class="sl-1" name="">
						<option value="">--勤務地--</option>
						<?php 
							if (!empty($ar_location)) {
								foreach ($ar_location as $location_item) {
									echo '<option value="'.$location_item->slug.'">'.$location_item->name.'</option>';
								}
							}
						?>
					</select>
				</div>
				<div class="box-sl">
					<select class="sl-1">
						<option value="">-- 職種 --</option>
						<?php 
							if (!empty($ar_skills)) {
								foreach ($ar_skills as $skill_item) {
									echo '<option value="'.$skill_item->slug.'">'.$skill_item->name.'</option>';
								}
							}
						?>
					</select>
				</div>
				<div class="box-sl">
					<select class="sl-1"  name="">
						<option value="">年収下限</option>
						<?php $salaries = array(100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000);
						foreach($salaries as $sal){
						?>                    
						<option value="<?php echo $sal ?>" <?php echo (isset($_GET['salary']) && $_GET['salary'] == $sal) ? 'selected' : '' ?>><?php echo $sal ?>万円</option>
						<?php }?>
					</select>
				</div>
				<div class="box-search">
					<input type="search" class="search-adv" name="" placeholder="Nhập từ khóa tìm kiếm">
				</div>
				<div class="submit">
					<input type="submit" value="" class="sub-adv" >
				</div>
				<a class="btn btn-orange w100p js-submit" href="#"><i class="fa fa-search" aria-hidden="true"></i> TÌM KIẾM</a>
			</div>
			<!--- /SEARCH ADVANCED -->
		</div>

		<div class="list-work row row-recrui">
			<!-- Loop data jobs -->
			<?php
				$argsJobs = array(
					'post_type'      => 'recruitment',
					'orderby'        => 'date',
					'hide_empty'     => false,
					'depth'          => 1,
					'post_status'    => 'publish',
					'order'          => 'DESC',
					'posts_per_page' => 12
				);
				$wp_query1 = new WP_Query( $argsJobs );
			?>
			<?php if ($wp_query1->have_posts()) : ?>
			<?php while ($wp_query1->have_posts()) : $wp_query1->the_post(); ?>
				<div class="list-work-item col-lg-3 col-md-4 col-sm-6 col-xs-6 col-full wow fadeInUp animated" data-wow-delay="0.4s">
					<a href="<?php echo the_permalink(); ?>" class="box-work" title="">
						<div class="box-work-img"><img src="<?php 
							if ( get_field('detail_image') ) {
								echo get_field('detail_image'); 
							}
							else {
								echo kyushoku_getTemplateUrl()."/images/recruitment/img-4.png";
							}
							?>" alt="<?php the_title() ?>" /></div>
						<?php
							$term_locations = get_the_terms($post->ID, 'job-location');
							$term_positions = get_the_terms($post->ID, 'job-position');
							$term_skills = get_the_terms($post->ID, 'job-skill');
						?>
						<figcaption><span><?php the_title(); ?></span></figcaption>
						<div class="cmt-work">
							<p class="cmt-map">
								<?php 
								if (is_array($term_locations)):
									foreach($term_locations as $location) :
										echo $location->name . ", ";
									endforeach;
								endif;
								?>
							</p>
							<p class="cmt-sal">
								<?php 
								if ( !is_null(get_field('salary')) ) :
									echo get_field('salary');
								endif;
								?>
							</p>
							<div class="tit-skill">
								<span>
									<?php
										echo __('[:ja]Skills[:en]Skills[:vi]Kỹ năng', 'kyushoku')
									?>
								</span>
							</div>
							<div class="list-skill">
								<?php
								if (is_array($term_skills)):
									foreach($term_skills as $skill_item) :
										echo "<span>". $skill_item->name ."</span>";
									endforeach;
								endif;
								?>
							</div>
							<span class="readmore">
								<?php 
									echo __('[:ja]Detail >>[:en]Detail >>[:vi]Xem chi tiết >>', 'kyushoku')
								?>
							</span>
						</div>
					</a>
				</div>

			<?php endwhile; ?>
			<?php endif; ?>
			<!-- /End Loop data jobs -->
		</div>
		
		<div class="row row-recrui">
			<!-- PAGINATION -->
			<div class="recrui-pagination">
				<ul>
					<li class="prev"><a href="#prev"><img src="<?php kyushoku_getTemplateUrl(); ?>/images/recruitment/icon-prev.png" alt="" /></a></li>
					<li class="number active"><a href="#1">1</a></li>
					<li class="number"><a href="#2">2</a></li>
					<li class="number"><a href="#3">3</a></li>
					<li class="next"><a href="#next"><img src="<?php kyushoku_getTemplateUrl(); ?>/images/recruitment/icon-next.png" alt="" /></a></li>
				</ul>
			</div>
			<!-- /PAGINATION -->
		</div>
	</form>
	

</section>
<!--- /CONTENT -->

<!-- SEARCH BOTTOM -->
<div class="search-bottom">
	<div class="container">
		<div class="wr-select-adv row row-recrui">
			<div class="box-sl">
				<select class="sl-1" name="">
					<option value="">--勤務地--</option>
						<?php 
							if (!empty($ar_location)) {
								foreach ($ar_location as $location_item) {
									echo '<option value="'.$location_item->slug.'">'.$location_item->name.'</option>';
								}
							}
						?>
				</select>
			</div>
			<div class="box-sl">
				<select class="sl-1">
					<option value="">-- 職種 --</option>
						<?php 
							if (!empty($ar_skills)) {
								foreach ($ar_skills as $skill_item) {
									echo '<option value="'.$skill_item->slug.'">'.$skill_item->name.'</option>';
								}
							}
						?>
				</select>
			</div>
			<div class="box-sl">
				<select class="sl-1"  name="">
					<option value="">年収下限</option>
						<?php $salaries = array(100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000);
						foreach($salaries as $sal){
						?>                    
						<option value="<?php echo $sal ?>" <?php echo (isset($_GET['salary']) && $_GET['salary'] == $sal) ? 'selected' : '' ?>><?php echo $sal ?>万円</option>
						<?php }?>
				</select>
			</div>
			<div class="box-search">
				<input type="search" class="search-adv" name="" placeholder="Nhập từ khóa tìm kiếm">
			</div>
			<div class="submit">
				<input type="submit" value="" class="sub-adv" >
			</div>
			<a class="btn btn-orange w100p js-submit" href="#"><i class="fa fa-search" aria-hidden="true"></i> TÌM KIẾM</a>
		</div>
	</div>
</div>
<!-- /SEARCH BOTTOM -->
<?php
get_footer();
?>
<script>
	new WOW().init();
</script>