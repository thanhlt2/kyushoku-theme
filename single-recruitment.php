<?php
    wp_enqueue_style('kyushoku-recruitment-animate', get_template_directory_uri() . '/css/recrui/animate.css', array(), '1.0', 'all');
    wp_enqueue_style('kyushoku-recruitment-page', get_template_directory_uri() . '/css/recrui/recruitment.css', array(), '1.0', 'all');
    wp_enqueue_style('kyushoku-recruitment-responsive', get_template_directory_uri() . '/css/recrui/responsive.css', array(), '1.0', 'all');
    wp_enqueue_style('kyushoku-recruitment-myriad', get_template_directory_uri() . '/css/recrui/myriad.css', array(), '1.0', 'all');
    wp_enqueue_style('kyushoku-recruitment-1', get_template_directory_uri() . '/css/recrui-detail/recruitment-detail.css', array(), '1.0', 'all');
    wp_enqueue_style('kyushoku-recruitment-2', get_template_directory_uri() . '/css/recrui-detail/recruitment-detail-responsive.css', array(), '1.0', 'all');
    wp_enqueue_style('kyushoku-recruitment-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', array(), '1.0', 'all');
    wp_enqueue_style('kyushoku-recruitment-3', get_template_directory_uri() . '/css/recrui-detail/owl.carousel.css', array(), '1.0', 'all');
    wp_enqueue_style('kyushoku-recruitment-4', get_template_directory_uri() . '/css/recrui-detail/owl.theme.default.css', array(), '1.0', 'all');
    wp_enqueue_style('kyushoku-recruitment-5', get_template_directory_uri() . '/css/recrui-detail/detail-responsive.css', array(), '1.0', 'all');
    get_header();
?>
    <?php
    while ( have_posts() ) : the_post();
        // <!--- wr-breadcrumb-sub -->
        get_template_part( 'template-parts/content-rec-detail', get_post_type() );

    endwhile; // End of the loop.
    ?>

<?php
    wp_enqueue_script( 'kyushoku-recruitment-script-wow', get_template_directory_uri() . '/js/recrui/wow.js', array(), '', true );
    wp_enqueue_script( 'kyushoku-recruitment-script-wow-sc', get_template_directory_uri() . '/js/recrui/wow-lib.js', array(), '', true );
    wp_enqueue_script( 'kyushoku-recruitment-script-wow-sc-1', get_template_directory_uri() . '/js/recrui-detail/owl.carousel.min.js', array(), '', true );
    wp_enqueue_script( 'kyushoku-recruitment-script', get_template_directory_uri() . '/js/recrui-detail/script-recrui-detail.js', array(), '', true );
    get_footer();
?>
<script>
    (function ($) {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 0,
            responsiveClass: true,
            responsive: {
                0: {
                items: 1,
                nav: true
                },
                502: {
                items: 2,
                nav: false
                },

                1199: {
                items: 3,
                nav: false,
                loop: false,
                margin: 10
                },
                1300: {
                items: 4,
                nav: true,
                loop: false,
                margin: 10
                }
            }
        });
        if ( $("#qtranslate-language-rec-chooser").length > 0) {
            $("#qtranslate-language-rec-chooser").addClass("dropdown-menu dropdown-menu-pc");
        }
    })(jQuery);
    
</script>

<script>
    new WOW().init();
</script>