<?php
/**
 * kyushoku-theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package kyushoku-theme
 */

/**
 * Include function-admin
 **/
if( file_exists(get_template_directory().'/inc/function-admin.php') ){
    require( get_template_directory() . '/inc/function-admin.php' );
}

/**
 * Include Enqueue scripts and styles.
 **/
if( file_exists(get_template_directory().'/inc/enqueue.php') ){
    require( get_template_directory().'/inc/enqueue.php' );
}

/**
 * Include Enqueue scripts and styles.
 **/
if( file_exists(get_template_directory().'/inc/cleanup.php') ){
    require( get_template_directory().'/inc/cleanup.php' );
}

/**
 * Define function for common
 *
 **/
if ( !function_exists('kyushoku_getTemplateUrl') ) {
	/**
	 * Function get template url
	 **/
	function kyushoku_getTemplateUrl(){
		echo get_template_directory_uri();
	}
}

if ( !function_exists('kyushoku_getMenu') ) {
	/**
	* The function to getting the menu depend on name of menu
	* @param array config
	* @return array menu
	**/
	function kyushoku_getMenu($menu) {
		wp_nav_menu($menu);
	}
}
if ( !function_exists('kyushoku_custom_navClassActive') ) {
	/**
	 * Function custom class active for Menu
	 **/
	function kyushoku_custom_navClassActive ($classes, $item) {
		$current_post_type = get_post_type_object(get_post_type($post->ID));
		$current_post_type_slug = $current_post_type->rewrite[slug];
		$menu_slug = strtolower(trim($item->url));
		if (strpos($menu_slug,$current_post_type_slug) !== false || in_array('current-menu-item', $classes) ) {
			$classes[] = 'active';
		
		}
		return $classes;

		// if (in_array('current-menu-item', $classes) ){
		// 	$classes[] = 'active ';
		// }
		// return $classes;
	}
	add_filter('nav_menu_css_class' , 'kyushoku_custom_navClassActive' , 10 , 2);
}

add_filter('acf/format_value_for_api', 'theme_format_value_for_api');
function theme_format_value_for_api($value) {
        return is_string($value) ? __($value) : $value;
}